﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class GameOver : MonoBehaviour  
{
    public Text text;
    // Start is called before the first frame update
    void Start()
    {
        text.text = PlayerPrefs.GetString("playerFinalScore");
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void exitGame() {
        Application.Quit();
    }
    public void restartGame()
    {
        SceneManager.LoadScene("SampleScene");
    }


}
