﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NpcBehaviour : MonoBehaviour
{

    public float speed;
    public GameObject npc;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += new Vector3(0,speed,0) * Time.deltaTime;
        InvokeRepeating("speedUp", 5f, 3f);
    }


    void speedUp() {
        speed = speed + 0.2f;
    }
}
