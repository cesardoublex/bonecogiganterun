﻿using UnityEngine.UI;
using UnityEngine;

public class Hud : MonoBehaviour
{
    public Text text;    
    private float score;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {        
        score += Time.deltaTime * 10;  
        text.text = score.ToString("f0");
    }

    public string finalScore() {    
       return   "Score: " + text.text;
    }
}
