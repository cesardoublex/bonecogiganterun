﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Npc : MonoBehaviour
{

    public float maxWidth;
    public float minWidth;

    public float rateSpawn;

    private float currentRateSpawn;

    public int maxNpc;

    public GameObject prefab;

    public List<GameObject> listNpc;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < maxNpc; i++) {
            GameObject tempNpc = Instantiate(prefab) as GameObject;
            listNpc.Add(tempNpc);
            tempNpc.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        currentRateSpawn += Time.deltaTime;

        if (currentRateSpawn > rateSpawn) {
            currentRateSpawn = 0;
            Spawn();
        }
       
    }

    //Fuction to create the npcs
    private void Spawn()
    {

        float randomPosition = Random.Range(minWidth, maxWidth);

        GameObject tempNpc = null;

        for (int i = 0; i < maxNpc; i++)
        {
            if (listNpc[i].activeSelf == false) {
                tempNpc = listNpc[i];
                break;
            }
        }

        if (tempNpc != null)
        {
            tempNpc.transform.position = new Vector3(randomPosition, -5.15f, transform.position.z);
            tempNpc.SetActive(true);
        }
    }
}
