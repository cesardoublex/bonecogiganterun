﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
	// Reference to Rigidbody2D component of the ball game object
	Rigidbody2D rb;

	// Range option so moveSpeedModifier can be modified in Inspector
	// this variable helps to simulate objects acceleration
	[Range(0.2f, 10f)]
	public float moveSpeedModifier = 0.5f;

	// Direction variables that read acceleration input to be added
	// as velocity to Rigidbody2d component
	float dirX, dirY;

	// Variable to allow or disallow movement when ball is alive or dead
	static bool moveAllowed;

	private Animator animator;

	public string playerFinalScore;

	// Use this for initialization
	void Start()
	{
		// Movement is allowed at the start
		moveAllowed = true;

		// Getting Rigidbody2D component of the ball game object
		rb = GetComponent<Rigidbody2D>();

		// Set  animation
		animator = GetComponent<Animator>();
		animator.SetBool("player_idle", true);
	}

	// Update is called once per frame
	void Update()
	{

		// Getting devices accelerometer data in X and Y direction
		// multiplied by move speed modifier
		dirX = Input.acceleration.x * moveSpeedModifier;


		if (Input.acceleration.x > 0f)
		{
			animator.SetBool("player_run", true);
			transform.eulerAngles = new Vector3(0f, 180f, 0f);
		}

		if (Input.acceleration.x < 0f)
		{
			animator.SetBool("player_run", true);
			transform.eulerAngles = new Vector3(0f, 0f, 0f);
		}

		if (Input.acceleration.x == 0f)
		{
			animator.SetBool("player_run", false);

		}

	}

     void OnTriggerEnter2D(Collider2D collider)
    {
		if (collider.tag == "Npc") {
			playerFinalScore = GameObject.Find("TimeText").GetComponent<Hud>().finalScore();
			PlayerPrefs.SetString("playerFinalScore", playerFinalScore);
			SceneManager.LoadScene("GameOver");
			
		}
    }


    void FixedUpdate()
	{
		// Setting a velocity to Rigidbody2D component according to accelerometer data
		if (moveAllowed)
			rb.velocity = new Vector2(-1*dirX, 0);

	}
	
}
