﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    private float length;
    private float startPosition;

    private Transform cam;

    public float parallaxEffect;

    void Start()
    {
        startPosition = transform.position.y;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
        cam = Camera.main.transform;
        Screen.orientation = ScreenOrientation.LandscapeLeft;
    }

    // Update is called once per frame
    void Update()
    {
        float rePosition = cam.transform.position.y * (1 - parallaxEffect);
        float distance = cam.transform.position.y * parallaxEffect;

        transform.position = new Vector3(transform.position.x, startPosition + distance, transform.position.z);
        startPosition += 0.005f;

        if (rePosition > startPosition + length / 2){
            startPosition += length;
        }
        else if (rePosition < startPosition - length / 2) {
            startPosition -= length;
        }
    }
}
